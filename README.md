# Struktur


1. Introduction
  * "Isabelle is a generic proof assistant." ... http://isabelle.in.tum.de/overview.html
2. Higher Order Logic, repetition(?) & examples
  * http://en.wikipedia.org/wiki/Higher-order_logic
  * http://isabelle.in.tum.de/dist/Isabelle2013-2/doc/tutorial.pdf
3. Introduction to "HOL", the functional programming language
  * http://isabelle.in.tum.de/dist/Isabelle2013-2/doc/prog-prove.pdf
  * Coding some examples from earlier
4. Possibilities, limitations ...
